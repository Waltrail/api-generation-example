import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';


/// This way we can add yaml files to generate API clients based on dio
@Openapi(
  additionalProperties: AdditionalProperties(pubName: 'petstore_api'),
  inputSpecFile: 'petstore_api.yaml',
  generatorName: Generator.dio,
  outputDirectory: 'api/petstore_api',
)
class PetStoreApi extends OpenapiGeneratorConfig {}
