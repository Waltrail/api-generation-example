import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:dio/dio.dart';
import 'package:dio_logger/dio_logger.dart';
import 'package:petstore_api/petstore_api.dart';

class PetstoreNetworkService {
  PetstoreNetworkService() {
    // Getting a reference to the Generate API
    final generatedAPI = PetstoreApi();

    // Adding additional dio interceptors for data logging
    generatedAPI.dio.interceptors.addAll([
      dioLoggerInterceptor,
    ]);

    // Getting an instances to call the pet store API
    petApi = generatedAPI.getPetApi();
    storeApi = generatedAPI.getStoreApi();
    userApi = generatedAPI.getUserApi();
  }

  late final PetApi petApi;
  late final StoreApi storeApi;
  late final UserApi userApi;

  // Methods to get data from Pets API

  /// Get [Pet] by petId
  ///
  /// Returns null on error
  Future<Pet?> getPetById(int id) async =>
      petApi.getPetById(petId: id).then((result) {
        return result.data;
      }).catchError((Object error) {
        // Do something on error
      });

  /// Adds new [Pet]
  ///
  /// Returns true if success
  Future<bool> addNewPet(Pet pet) async =>
      petApi.addPet(body: pet).then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Update an existing [Pet]
  ///
  /// Returns true if success
  Future<bool> updatePet(Pet pet) async =>
      petApi.updatePet(body: pet).then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Updates a [Pet] in the store with form data
  ///
  /// Returns true if success
  Future<bool> updatePetWithForm(
          int petId, String? name, String? status) async =>
      petApi
          .updatePetWithForm(
        petId: petId,
        name: name,
        status: status,
      )
          .then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Deletes a [Pet] in the store
  ///
  /// Returns true if success
  Future<bool> deletePet(
    int petId,
    String apiKey,
  ) async =>
      petApi
          .deletePet(
        petId: petId,
        apiKey: apiKey,
      )
          .then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Deletes a [Pet] in the store
  ///
  /// Returns true if success
  Future<bool> uploadPetImage(
    int petId,
    String? additionalMetadata,
    MultipartFile? file,
  ) async =>
      petApi
          .uploadFile(
        petId: petId,
        additionalMetadata: additionalMetadata,
        file: file,
      )
          .then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Find all [Pet]s in the store with the same status
  ///
  /// Returns null on error
  Future<List<Pet>?> findPetsByStatus(
    List<String> statuses,
  ) async =>
      petApi.findPetsByStatus(status: BuiltList.from(statuses)).then((result) {
        return result.data?.toList();
      }).catchError((Object error) {
        // Do something on error
      });

  // ...
  // ...
  // Accordingly we can create every other method for [storeApi] and [userApi]
  // It's better practice to separate them into different files
}
