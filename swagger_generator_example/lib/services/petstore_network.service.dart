import 'dart:io';

import 'package:swagger_generator_example/swagger_generated_api/petstore_api.swagger.dart';

class PetstoreNetworkService {
  PetstoreNetworkService() {
    // Creating an instances to call the pet store API
    petApi = PetstoreApi.create();
  }

  late final PetstoreApi petApi;

  // Methods to get data from Pets API

  /// Get [Pet] by petId
  ///
  /// Returns null on error
  Future<Pet?> getPetById(int id) async =>
      petApi.petPetIdGet(petId: id).then((result) {
        return result.body;
      }).catchError((Object error) {
        // Do something on error
      });

  /// Adds new [Pet]
  ///
  /// Returns true if success
  Future<bool> addNewPet(Pet pet) async =>
      petApi.petPost(body: pet).then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Update an existing [Pet]
  ///
  /// Returns true if success
  Future<bool> updatePet(Pet pet) async =>
      petApi.petPut(body: pet).then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Updates a [Pet] in the store with form data
  ///
  /// Returns true if success
  Future<bool> updatePetWithForm(
          int petId, String? name, String? status) async =>
      petApi
          .petPetIdPost(
        petId: petId,
        name: name,
        status: status,
      )
          .then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Deletes a [Pet] in the store
  ///
  /// Returns true if success
  Future<bool> deletePet(
    int petId,
    String apiKey,
  ) async =>
      petApi
          .petPetIdDelete(
        petId: petId,
        apiKey: apiKey,
      )
          .then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Deletes a [Pet] in the store
  ///
  /// Returns true if success
  Future<bool> uploadPetImage(
    int petId,
    String? additionalMetadata,
    List<String>? file,
  ) async =>
      petApi
          .petPetIdUploadImagePost(
        petId: petId,
        additionalMetadata: additionalMetadata,
        file: file,
      )
          .then((result) {
        return result.statusCode == HttpStatus.ok;
      }).catchError((Object error) {
        return false;
      });

  /// Find all [Pet]s in the store with the same status
  ///
  /// Returns null on error
  Future<List<Pet>?> findPetsByStatus(
    PetFindByStatusGetStatus? status,
  ) async =>
      petApi.petFindByStatusGet(status: status).then((result) {
        return result.body;
      }).catchError((Object error) {
        // Do something on error
      });

  // ...
  // ...
  // Accordingly we can create every other method for [storeApi] and [userApi]
  // It's better practice to separate them into different files
}
